from django.apps import AppConfig


class FrontendServiceConfig(AppConfig):
    name = "frontend_service"
