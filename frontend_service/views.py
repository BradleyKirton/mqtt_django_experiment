from django.views.generic import TemplateView

import requests
import paho.mqtt.client as mqtt


client = mqtt.Client()
client.connect("localhost", 1883, 60)
client.loop_start()


class FrontendView(TemplateView):
    template_name = "frontend_service/index.html"


class BroadcastView(TemplateView):
    template_name = "frontend_service/broadcast.html"

    def get(self, request):
        infot = client.publish("class", "bar", qos=2)
        return super().get(request)


# View functions
frontend_view = FrontendView.as_view()
broadcast_view = BroadcastView.as_view()
