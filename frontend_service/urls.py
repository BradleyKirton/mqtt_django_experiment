from django.urls import path
from frontend_service.views import frontend_view, broadcast_view


urlpatterns = [
    path("", frontend_view, name="frontend"),
    path("broadcast/", broadcast_view, name="auth-view"),
]
