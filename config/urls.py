from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include(("auth_service.urls", "auth_service"), namespace="api")),
    path(
        "", include(("frontend_service.urls", "frontend_service"), namespace="frontend")
    ),
]
