from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response


class AuthServiceView(APIView):
    """Mock auth service"""

    def post(self, request: Request) -> None:
        """Just reponse with 200."""

        print("POST to auth service")
        return Response({"ok": True})

    def get(self, request: Request) -> None:
        """Just reponse with 200."""

        print("GET to auth service")
        return Response({"ok": True})


# View functions
auth_service_view = AuthServiceView.as_view()
