from django.urls import path
from auth_service.views import auth_service_view


urlpatterns = [path("auth/", auth_service_view, name="auth-view")]
